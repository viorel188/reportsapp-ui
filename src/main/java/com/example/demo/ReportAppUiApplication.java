package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReportAppUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportAppUiApplication.class, args);
	}

}
