package com.example.demo;

public class SignUpCredentials {
	private String signupusername;
	private String signuppassword;
	private String signuprepassword;
	
	public String getSignupusername() {
		return signupusername;
	}
	public String getSignuppassword() {
		return signuppassword;
	}
	public String getSignuprepassword() {
		return signuprepassword;
	}
	public void setSignupusername(String signupusername) {
		this.signupusername = signupusername;
	}
	public void setSignuppassword(String signuppassword) {
		this.signuppassword = signuppassword;
	}
	public void setSignuprepassword(String signuprepassword) {
		this.signuprepassword = signuprepassword;
	}
}
