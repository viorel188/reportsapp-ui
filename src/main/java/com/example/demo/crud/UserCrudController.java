package com.example.demo.crud;

import static com.example.demo.ViewsController.BE_BASE_URL;
import static com.example.demo.ViewsController.LOGGEDIN_USER_ID;
import static com.example.demo.ViewsController.USER_TOKEN;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Controller
public class UserCrudController {
	
	@PutMapping(value = "/users/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public String updateUserPassword(@PathVariable long id,
			@RequestBody ChangePasswordData changePasswordData, HttpSession session) {

		PasswordChangeStatus reqStatus = validatePasswordChangeRequest(id, changePasswordData, session);
		if( reqStatus != PasswordChangeStatus.OK ) {
			return reqStatus.getStatus();
		}

		RestTemplate restTemplate = new RestTemplate();
		String url = BE_BASE_URL + "/users/" + id;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("token", session.getAttribute(USER_TOKEN).toString());

		HttpEntity<ChangePasswordData> request = new HttpEntity<ChangePasswordData>(changePasswordData, headers);
		try {
			ResponseEntity<String> updatedPass = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
			System.out.println("After upd pass: " + updatedPass.getBody());
			return updatedPass.getBody();
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			System.out.println("Except msg: " + e.getMessage());
			return e.getMessage();
		}
	}

	private PasswordChangeStatus validatePasswordChangeRequest(long id, ChangePasswordData changePasswordData,
			HttpSession session) {
		if (session.getAttribute(LOGGEDIN_USER_ID) == null) {
			return PasswordChangeStatus.PlsLogIn;
		}
		long userId = (long) session.getAttribute(LOGGEDIN_USER_ID);
		if (id != userId) {
			return PasswordChangeStatus.UserIdsMismatch;
		}
		if (!changePasswordData.getNewPassword().equals(changePasswordData.getNewRePassword())) {
			return PasswordChangeStatus.NewPassAndReNewPassMismatch;
		}

		return PasswordChangeStatus.OK;
	}
	
	enum PasswordChangeStatus {
		PlsLogIn("Please log in!"),
		UserIdsMismatch("You can change only your user password!"),
		NewPassAndReNewPassMismatch("New password and repeated new password don't match!"),
		OK("Ok!");
		
		String status;
		PasswordChangeStatus(String status) {
			this.status = status;
		}
		public String getStatus() {
			return status;
		}
	}
}
