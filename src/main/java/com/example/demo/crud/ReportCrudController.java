package com.example.demo.crud;

import static com.example.demo.ViewsController.BE_BASE_URL;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class ReportCrudController {

	@DeleteMapping(value="/reports/{id}")
	@ResponseBody
	public String deleteReport(@PathVariable long id) {
		String url = BE_BASE_URL + "/reports/" + id;
	     
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(url);
	    return "del";
	}
}
