package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.crud.ChangePasswordData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class ViewsController {

	final private String LOGGEDIN_USERNAME = "loggedUser";
	final public static String LOGGEDIN_USER_ID = "loggedUserId";
	final public static String USER_TOKEN = "userToken";
	final public static String BE_BASE_URL = "http://localhost:8281";

	final private String REQUEST_REPORT_PAGE = "reports.jsp";
	final private String VIEW_REPORTS_PAGE = "viewReports.jsp";
	final private String INDEX_PAGE = "index.jsp";

	@GetMapping(value = "/")
	String index() {
		return INDEX_PAGE;
	}

	@PostMapping(value = "/login")
	ModelAndView login(Credentials credentials, HttpSession httpSession) {
		/** TODO: validation for credentials */
		System.out.println("Reports for " + credentials.getUsername());

		String loginUrl = BE_BASE_URL + "/login2";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject userJsonObject = new JSONObject();
		userJsonObject.put("username", credentials.getUsername());
		userJsonObject.put("password", credentials.getPassword());

		HttpEntity<String> request = new HttpEntity<String>(userJsonObject.toString(), headers);
		try {
			ResponseEntity<LoginResponse> result = restTemplate.postForEntity(loginUrl, request, LoginResponse.class);

			LoginResponse lrsp = (LoginResponse) result.getBody();
			System.out.println("Result: " + lrsp.getUsername() + ", " + lrsp.getToken() + ", " + lrsp.getMessage()
					+ result.getStatusCode());

			LoginResponse loginRsp = (LoginResponse) result.getBody();
			httpSession.setAttribute(LOGGEDIN_USERNAME, loginRsp.getUsername());
			httpSession.setAttribute(LOGGEDIN_USER_ID, loginRsp.getId());
			httpSession.setAttribute(USER_TOKEN, loginRsp.getToken());
			ModelAndView reportsView = new ModelAndView(REQUEST_REPORT_PAGE);
			System.out.println("ok");
			return reportsView;
		} catch (HttpClientErrorException e) {
			ModelAndView invalidLoginView = new ModelAndView(INDEX_PAGE);
			invalidLoginView.addObject("err_login", e.getMessage());
			return invalidLoginView;
		}
	}

	@PostMapping(value = "/signup")
	ModelAndView signup(SignUpCredentials credentials, HttpSession httpSession) {
		if (!credentials.getSignuppassword().equals(credentials.getSignuprepassword())) {
			ModelAndView errView = new ModelAndView(INDEX_PAGE);
			errView.addObject("err_signup", "Passwords don't match!");
			return errView;
		}

		String signUpUrl = BE_BASE_URL + "/signup2";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject userJsonObject = new JSONObject();
		userJsonObject.put("username", credentials.getSignupusername());
		userJsonObject.put("password", credentials.getSignuppassword());

		HttpEntity<String> request = new HttpEntity<String>(userJsonObject.toString(), headers);
		try {
			ResponseEntity<LoginResponse> result = restTemplate.postForEntity(signUpUrl, request, LoginResponse.class);
			LoginResponse loginRsp = (LoginResponse) result.getBody();
			httpSession.setAttribute(LOGGEDIN_USERNAME, loginRsp.getUsername());
			httpSession.setAttribute(LOGGEDIN_USER_ID, loginRsp.getId());
			httpSession.setAttribute(USER_TOKEN, loginRsp.getToken());

			System.out.println("Result: " + loginRsp.getUsername() + " " + loginRsp.getToken() + " "
					+ loginRsp.getMessage() + result.getStatusCode());
			ModelAndView reportsView = new ModelAndView(REQUEST_REPORT_PAGE);
			System.out.println("signUp ok");
			return reportsView;
		} catch (HttpServerErrorException e) {
			String detailMsg = e.getMessage();
			detailMsg = detailMsg.substring(detailMsg.indexOf('{'), detailMsg.indexOf('}') + 1);
			try {
				LoginResponse errLogin = new ObjectMapper().readValue(detailMsg, LoginResponse.class);
				ModelAndView invalidLoginView = new ModelAndView(INDEX_PAGE);
				invalidLoginView.addObject("err_login", errLogin.getMessage());
				return invalidLoginView;
			} catch (JsonProcessingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ModelAndView errView = new ModelAndView(INDEX_PAGE);
			errView.addObject("err_signup", "Something went wrong!");
			return errView;
		}
	}

	@GetMapping("/logout")
	public String logout(HttpSession session) {
		if (session.getAttribute(USER_TOKEN) != null && session.getAttribute(LOGGEDIN_USERNAME) != null) {
			invalidateToken(session.getAttribute(USER_TOKEN).toString(),
					session.getAttribute(LOGGEDIN_USERNAME).toString());
		}
		session.removeAttribute(LOGGEDIN_USERNAME);
		session.removeAttribute(LOGGEDIN_USER_ID);
		session.removeAttribute(USER_TOKEN);
		session.invalidate();
		System.out.println("logged out!");
		return INDEX_PAGE;
	}

	private void invalidateToken(String token, String username) {
		// post request to TestController->invalidateToken
		// periodically, blacklisttoken table should be cleaned
		String invalidateTokenURL = BE_BASE_URL + "/invalidateToken";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject userJsonObject = new JSONObject();
		userJsonObject.put("token", token);
		userJsonObject.put("username", username);

		HttpEntity<String> request = new HttpEntity<String>(userJsonObject.toString(), headers);
		restTemplate.postForEntity(invalidateTokenURL, request, String.class);
	}

	@PostMapping("/sendReportType")
	public ModelAndView sendReportTypeForGeneration(HttpServletRequest request1, HttpSession session) {
		if (session.getAttribute(LOGGEDIN_USERNAME) == null || session.getAttribute(USER_TOKEN) == null) {
			ModelAndView invalidLoginView = new ModelAndView(INDEX_PAGE);
			invalidLoginView.addObject("err_login", "Please login!");
			return invalidLoginView;
		}
		String reportType = request1.getParameter("reportType");
		System.out.println("Requested report: " + reportType + ", for user: " + session.getAttribute(LOGGEDIN_USERNAME));

		String requestReportUrl = BE_BASE_URL + "/generateReport";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("token", session.getAttribute(USER_TOKEN).toString());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("reportType", reportType);
		jsonObject.put("user", session.getAttribute(LOGGEDIN_USERNAME));
		HttpEntity<String> request2 = new HttpEntity<String>(jsonObject.toString(), headers);

		try {
			ResponseEntity<String> result = restTemplate.postForEntity(requestReportUrl, request2, String.class);
			ModelAndView reportsView = new ModelAndView(REQUEST_REPORT_PAGE);
			reportsView.addObject("report_pending", result.getBody());
			return reportsView;
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			ModelAndView errView = new ModelAndView(REQUEST_REPORT_PAGE);
			errView.addObject("err_requesting_report", e.getMessage());
			return errView;
		}
	}

	@GetMapping(value = "/reports")
	public ResponseEntity<?> getReports(HttpSession session, HttpServletResponse response) {
		if (session.getAttribute(LOGGEDIN_USERNAME) != null && session.getAttribute(USER_TOKEN) != null) {
			String username = (String) session.getAttribute(LOGGEDIN_USERNAME);
			RestTemplate restTemplate = new RestTemplate();
			String url = BE_BASE_URL + "/getReportsForUser/" + username;

			HttpHeaders headers = new HttpHeaders();
			headers.set("token", session.getAttribute(USER_TOKEN).toString());
			HttpEntity requestEntity = new HttpEntity(headers);
			try {
				ResponseEntity<Report[]> reports = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
						Report[].class);
				return reports;
			} catch (HttpClientErrorException e) {
				return new ResponseEntity<String>("Please log in!", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Please log in!", HttpStatus.BAD_REQUEST);
	}
}
