package com.example.demo;

import java.sql.Timestamp;

public class Report {
	private long idReport;
	private String type;
	private String status;
	private Timestamp created;
	private Timestamp updated;
	private long idUser;
	
	public Report() {
		super();
	}
	public Report(long idReport, String type, String status, Timestamp created, Timestamp updated, long idUser) {
		super();
		this.idReport = idReport;
		this.type = type;
		this.status = status;
		this.created = created;
		this.updated = updated;
		this.idUser = idUser;
	}
	public long getIdReport() {
		return idReport;
	}
	public void setIdReport(long idReport) {
		this.idReport = idReport;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	@Override
	public String toString() {
		return "Report [idReport=" + idReport + ", type=" + type + ", status=" + status + ", created=" + created
				+ ", updated=" + updated + ", idUser=" + idUser + "]\n";
	}
}
