<!DOCTYPE html>
<html>
<head>
	<title>Page Title</title>
	<script src="./js/jquery-3.5.1.js"></script>
	<link href="bootstrap\bootstrap-4.4.1-dist\css\bootstrap.min.css" rel='stylesheet'>
	<script src="bootstrap\bootstrap-4.4.1-dist\js\bootstrap.min.js"></script>
</head>
<body>
<% if( session.getAttribute("loggedUser")!=null) { %>

	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Reports</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="/reports.jsp">Request a report</a></li>
	      <li><a href="/viewReports.jsp">View reports</a></li>
	      <li><a href="/changePassword.jsp">Change password</a></li>
	      <li><a href="/logout">Logout</a></li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	  <div class="row">
	    <div class="col-sm-10">
			Hello, <%=session.getAttribute("loggedUser")%>!
		</div>
	  </div>
	  <br><hr>
	  <div class="row">
	 	<%if(request.getAttribute("report_pending")!=null) { %>
			<span style="color:green"><%=request.getAttribute("report_pending") %></span>
		<%} %>
	  </div>
	  <br><hr>
	  <div class="row">
	    <div class="col-sm-12">
	    	Request a report:<br>
	    	<form action="/sendReportType" method="post">
	    		Report type: <select name="reportType">
					<option value="a">a</option>
					<option value="b">b</option>
					<option value="c">c</option>
	    		</select>
	    		<input type="submit" value="Generate new report"/>
				<%if(request.getAttribute("err_requesting_report")!=null) { %>
					<span style="color:red"><%=request.getAttribute("err_requesting_report") %></span>
				<%} %>
	    	</form>
	    </div>
	  </div>
	  <br>
	</div>
<script>
<%} else { 
	response.sendRedirect("/index.jsp");
}%>

</body>
</html>