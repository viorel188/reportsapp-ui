<!DOCTYPE html>
<html>
<head>
	<title>Page Title</title>
	<script src="./js/jquery-3.5.1.js"></script>
	<link href="bootstrap\bootstrap-4.4.1-dist\css\bootstrap.min.css" rel='stylesheet'>
	<script src="bootstrap\bootstrap-4.4.1-dist\js\bootstrap.min.js"></script>
</head>
<body>
<% if( session.getAttribute("loggedUser")!=null) { %>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Reports</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="/reports.jsp">Request a report</a></li>
	      <li><a href="/viewReports.jsp">View reports</a></li>
	      <li><a href="/changePassword.jsp">Change password</a></li>
	      <li><a href="/logout">Logout</a></li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	  <div class="row">
	    <div class="col-sm-10">
			Hello, <%=session.getAttribute("loggedUser")%>!
		</div>
	  </div>
	  <div class="row">
	  	Enter current password: <input type="password" name="currentPassword" required /><br>
	  	Enter new password:     <input type="password" name="newPassword" required /><br>
	  	Reenter new password:   <input type="password" name="newRePassword" required /><br>
	  	<input type="button" value="Change password" onclick="changePassword()" />
	  </div>
	  <div class="row">
		<span style="color:red" id="changePasswordStatus"></span>
	  </div>
	</div>
<%} else { 
	response.sendRedirect("/index.jsp");
}%>

<script type="text/javascript">
function changePassword() {
	updateUrl = "/users/<%=session.getAttribute("loggedUserId").toString()%>";
	var updatePassData = JSON.stringify(
			{
				currentPassword : $('input[name ="currentPassword"]').val(),
				newPassword : $('input[name ="newPassword"]').val(),
				newRePassword : $('input[name ="newRePassword"]').val()
			}
		);
	console.log("Current pass: " + updatePassData);
	$.ajax({
		  type: 'PUT',
		  url: updateUrl,
		  contentType: 'application/json',
		  data: updatePassData,
		  success: function(responseData) {
			  $('#changePasswordStatus').text(responseData);
		  },
		  error: function(jqXhr, textStatus, errorMsg) {
		    console.log('Error updating password' + errorMsg + "  status: " + textStatus + "  jqXhr: " + jqXhr);
		  }
		});
}
</script>
</body>
</html>