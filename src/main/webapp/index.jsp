<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<script src="./js/jquery-3.5.1.js"></script>
<link href="bootstrap\bootstrap-4.4.1-dist\css\bootstrap.min.css" rel='stylesheet'>
<script src="bootstrap\bootstrap-4.4.1-dist\js\bootstrap.min.js"></script>
</head>
<body>
<br><br>
<% if( session.getAttribute("loggedUser")==null) { %>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
		<h4>Login</h4>
		<form action="/login" method="post">
		User: <input type="text" name="username" required/><br>
		Password: <input type="password" name="password" required/><br>
		<input type="submit" value="login"/><br>
		</form><br>
		<%if(request.getAttribute("err_login")!=null) { %>
			<span style="color:red"><%=request.getAttribute("err_login") %></span>
		<%} else
		if(request.getHeader("requestError")!=null) { 
		%>
			<span style="color:red"><%=request.getHeader("requestError") %></span>
		<%} %>
		<hr>
	</div>
	
	<div class="col-sm-6">
		<h4>SignUp</h4>
		<form action="/signup" method="post">
		Username: <input type="text" name="signupusername" required/><br>
		Password: <input type="password" name="signuppassword" required/><br>
		Repeat password: <input type="password" name="signuprepassword" required/><br>
		<input type="submit" value="signup"/><br>
		</form><br>
		<%if(request.getAttribute("err_signup")!=null) { %>
			<span style="color:red"><%=request.getAttribute("err_signup") %></span>
		<%} %>
	</div>
  </div>
</div>
	
<%} else {
	response.sendRedirect("/reports.jsp"); 
}%>

</body>
</html>