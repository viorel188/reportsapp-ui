<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<script src="./js/jquery-3.5.1.js"></script>
<link href="bootstrap\bootstrap-4.4.1-dist\css\bootstrap.min.css" rel='stylesheet'>
<script src="bootstrap\bootstrap-4.4.1-dist\js\bootstrap.min.js"></script>

<style>
table, th {
  border: 1px solid black;
}
</style>
</head>
<body>
<% if( session.getAttribute("loggedUser")!=null) { %>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Reports</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/reports.jsp">Request a report</a></li>
      <li><a href="/viewReports.jsp">View reports</a></li>
      <li><a href="/changePassword.jsp">Change password</a></li>
      <li><a href="/logout">Logout</a></li>
    </ul>
  </div>
</nav>

<div class="container">
  <div class="row">
    <div class="col-sm-10">
		Hello, <%=session.getAttribute("loggedUser")%>!
		<%} else { 
			response.sendRedirect("/index.jsp");
		}%>
	</div>
  </div>
  <br><hr>
  <div class="row">
  	<div class="col-sm-12">
  		<table id="reportsTable">
  		  <tr>
		    <th>Report ID</th>
		    <th>Last updated</th>
		    <th>Type</th>
		    <th>Status</th>
		    <th>Delete report</th>
		  </tr>
  		</table>
  	</div>
  </div>
</div>

<script>
$( document ).ready( showReports );
function showReports() {
	$.ajax({
		  type: 'GET',
		  url: '/reports',
		  dataType: 'json',
		  success: function(jsonData) {
		    $.each(jsonData, function(i, report){
				//console.log(i + ": " + report.idReport + ", " + report.updated + ", " + report.status);
				var removeReportButton = "<center><button onclick=\"deleteReport(" + report.idReport + ")\">remove</button></center>";
				$("#reportsTable").append(
						"<tr><td>" + report.idReport + "</td>" +
						"<td>" + report.updated + "</td>" +
						"<td>" + report.type + "</td>" +
						"<td>" + report.status + "</td>" +
						"<td>" + removeReportButton + "</td></tr>");
			    });

		  },
		  error: function() {
		    alert('Error loading ');
		  }
		});
}

function deleteReport(reportId) {
	var deleteReportUrl = "/reports/" + reportId;
	console.log("Report id: " + reportId);
	$.ajax({
	  type: 'DELETE',
	  url: deleteReportUrl,
	  success: function() {
		  console.log("report " + reportId + "deleted");
	  },
	  error: function(jqXhr, textStatus, errorMsg) {
	    console.log('Error deleting report ' + errorMsg + "  status: " + textStatus + "  jqXhr: " + jqXhr);
	  }
	});
}
</script>